﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.DetalleCompra>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Compras</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Menu ID="Menu" runat="server" StaticSelectedStyle-BackColor="#539DC2" StaticSelectedStyle-ForeColor="White"
		Orientation="Horizontal" CssClass="Pestañas">
		<Items>
			<asp:MenuItem Text="Registrar compras" Value="1"></asp:MenuItem>

		</Items>
	</asp:Menu>
	<hr />
	<asp:Table ID="TablaCompras" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.NombreProveedor) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.NombreProveedor, new SelectList(
					new List<Object>{
						new { value = 0, text = "Supermercado CENTROSUR"},
						new { value = 1, text = "Surtiplaza Florencia"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Fecha) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Fecha) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.NombreProducto) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.NombreProducto, new SelectList(
					new List<Object>{
						new { value = 0, text = "Arroz Diana"},
						new { value = 1, text = "2"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Presentacion) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Presentacion, new SelectList(
					new List<Object>{
						new { value = 0, text = "Libra"},
						new { value = 1, text = "Kilo"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Cantidad) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Cantidad) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.ValorUnitario) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.ValorUnitario) %>
			</asp:TableCell>
<%--			<asp:TableCell>
				<asp:ImageButton ID="AñadirDetalle" runat="server" ImageUrl="~/Content/Images/Iconos/ad.png" />
			</asp:TableCell>--%>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Subtotal) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Subtotal) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.ValorTotal) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.ValorTotal) %>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<asp:Panel ID="PanelGrillaDetalle" runat="server">
		<asp:GridView ID="GrillaDetalle" runat="server"
			Visible="true">
			<HeaderStyle CssClass="Header" />
			<RowStyle CssClass="Rows" />
			<Columns>
				<%--<asp:CommandField ShowDeleteButton="true" ButtonType="Image" DeleteImageUrl="Estilo/Images/Iconos/caneca.png" ControlStyle-Width="16px" ControlStyle-Height="16px" />--%>
			</Columns>
		</asp:GridView>
	</asp:Panel>
	<hr />
	<asp:Panel ID="PanelBotones" runat="server" HorizontalAlign="Center" CssClass="PanelBotones">
		<%: Html.ActionLink("Guardar", "Compras", "Formularios") %>
		<%: Html.ActionLink("Editar", "Compras", "Formularios") %>
		<%: Html.ActionLink("Eliminar", "Compras", "Formularios") %>
	</asp:Panel>
</asp:Content>
