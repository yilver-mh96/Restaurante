﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.Producto>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Productos</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<asp:Menu ID="Menu" runat="server" StaticSelectedStyle-BackColor="#539DC2" StaticSelectedStyle-ForeColor="White"
		Orientation="Horizontal" CssClass="Pestañas">
		<Items>
			<asp:MenuItem Text="Productos" Value="1"></asp:MenuItem>
			<asp:MenuItem Text=" | " Selectable="false" Selected="false"></asp:MenuItem>
			<asp:MenuItem Text="Categorías" Value="2"></asp:MenuItem>
			<asp:MenuItem Text=" | " Selectable="false" Selected="false"></asp:MenuItem>
			<asp:MenuItem Text="Unidades de presentación" Value="3"></asp:MenuItem>
		</Items>
	</asp:Menu>
	<hr />
	<asp:Table ID="TablaProductos" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.NombreProducto) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.NombreProducto) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Categoria) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Categoria, new SelectList(
					new List<Object>{
						new { value = 0, text = "Alimentos"},
						new { value = 1, text = "Ornamentación"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Subcategoria) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Subcategoria, new SelectList(
					new List<Object>{
						new { value = 0, text = "Granos"},
						new { value = 1, text = "Abarrotes"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<hr />
	<asp:Panel ID="PanelBotones" runat="server" HorizontalAlign="Center" CssClass="PanelBotones">
		<%: Html.ActionLink("Guardar", "Productos", "Formularios") %>
		<%: Html.ActionLink("Editar", "Productos", "Formularios") %>
		<%: Html.ActionLink("Eliminar", "Productos", "Formularios") %>
	</asp:Panel>

</asp:Content>
