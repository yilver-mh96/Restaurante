﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.Persona>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Usuario</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Menu ID="Menu" runat="server" StaticSelectedStyle-BackColor="#539DC2" StaticSelectedStyle-ForeColor="White"
		Orientation="Horizontal" CssClass="Pestañas">
		<Items>
			<asp:MenuItem Text="Registrar usuarios" Value="1"></asp:MenuItem>
			<asp:MenuItem Text=" | " Selectable="false" Selected="false"></asp:MenuItem>
			<asp:MenuItem Text="Roles" Value="2"></asp:MenuItem>
			<asp:MenuItem Text=" | " Selectable="false" Selected="false"></asp:MenuItem>
			<asp:MenuItem Text="Permisos" Value="3"></asp:MenuItem>
			<asp:MenuItem Text=" | " Selectable="false" Selected="false"></asp:MenuItem>
			<asp:MenuItem Text="Horarios de trabajo" Value="4"></asp:MenuItem>
			<asp:MenuItem Text=" | " Selectable="false" Selected="false"></asp:MenuItem>
			<asp:MenuItem Text="Salarios" Value="5"></asp:MenuItem>
		</Items>
	</asp:Menu>
	<hr />
	<% using (Html.BeginForm()) { %>
	<%: Html.AntiForgeryToken() %>

	<asp:Table ID="TablaDatosPersonales" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.Cedula) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.Cedula) %></asp:TableCell>
			<asp:TableCell><%: Html.LabelFor(m => m.Rol) %></asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Rol, new SelectList(
					new List<Object>{
						new { value = 0, text = "Administrador"},
						new { value = 1, text = "Empleado"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.Nombre1) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.Nombre1) %></asp:TableCell>
			<asp:TableCell><%: Html.LabelFor(m => m.Nombre2) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.Nombre2) %></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.Apellido1) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.Apellido1) %></asp:TableCell>
			<asp:TableCell><%: Html.LabelFor(m => m.Apellido2) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.Apellido2) %></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.Genero) %></asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Genero, new SelectList(
					new List<Object>{
						new { value = 0, text = "Masculino"},
						new { value = 1, text = "Femenino"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
			<asp:TableCell><%: Html.LabelFor(m => m.FechaNac) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.FechaNac) %></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.Telefono) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.Telefono) %></asp:TableCell>
			<asp:TableCell><%: Html.LabelFor(m => m.Direccion) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.Direccion) %></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.Correo) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.Correo) %></asp:TableCell>
			<asp:TableCell><%: Html.LabelFor(m => m.Profesion) %></asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Profesion, new SelectList(
					new List<Object>{
						new { value = 0, text = "Docente"},
						new { value = 1, text = "Estudiante"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.Departamento) %></asp:TableCell>
			<asp:TableCell><%= Html.DropDownList("Departamento", ViewData["dpto"] as SelectList)%></asp:TableCell>
			<asp:TableCell><%: Html.LabelFor(m => m.Ciudad) %></asp:TableCell>
			<asp:TableCell><%: Html.DropDownList("Ciudad", new MultiSelectList(new[] {"Suaza", "Neiva"}))%></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.NUsuario) %></asp:TableCell>
			<asp:TableCell><%: Html.TextBoxFor(m => m.NUsuario) %></asp:TableCell>
			<asp:TableCell><%: Html.LabelFor(m => m.Contrasenia) %></asp:TableCell>
			<asp:TableCell><%: Html.PasswordFor(m => m.Contrasenia) %></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell><%: Html.LabelFor(m => m.ConfirmContrasenia) %></asp:TableCell>
			<asp:TableCell><%: Html.PasswordFor(m => m.ConfirmContrasenia) %></asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<hr />
	<asp:Panel ID="PanelBotones" runat="server" HorizontalAlign="Center" CssClass="PanelBotones">
		<button type="submit" value="Registrarse">Guardar</button>
		<button type="submit" value="Registrarse">Editar</button>
		<button type="submit" value="Registrarse">Eliminar</button>
		<%--<%: Html.ActionLink("Guardar", "Usuario", "Formularios") %>
		<%: Html.ActionLink("Editar", "Usuario", "Formularios") %>
		<%: Html.ActionLink("Eliminar", "Usuario", "Formularios") %>--%>
	</asp:Panel>
	<center>
		<h2><%: ViewBag.Message %></h2>
	</center>
	<fieldset class="Validaciones">
		<legend>Corrija los siguientes errores:</legend>
		<%: Html.ValidationSummary() %>
	</fieldset>
	<% } %>
</asp:Content>
