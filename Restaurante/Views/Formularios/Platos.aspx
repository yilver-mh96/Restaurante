﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.Plato>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Platos</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Menu ID="Menu" runat="server" StaticSelectedStyle-BackColor="#539DC2" StaticSelectedStyle-ForeColor="White"
		Orientation="Horizontal" CssClass="Pestañas">
		<Items>
			<asp:MenuItem Text="Platos" Value="1"></asp:MenuItem>
			<asp:MenuItem Text=" | " Selectable="false" Selected="false"></asp:MenuItem>
			<asp:MenuItem Text="Tipos de plato" Value="2"></asp:MenuItem>
		</Items>
	</asp:Menu>
	<hr />
	<asp:Table ID="TablaPlatos" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.NombrePlato) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.NombrePlato) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.TipoPlato) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.TipoPlato, new SelectList(
					new List<Object>{
						new { value = 0, text = "Corriente"},
						new { value = 1, text = "Ejecutivo"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Precio) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Precio) %>
			</asp:TableCell>
			<%--<asp:TableCell>
				<%: Html.LabelFor(m => m.Correo) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Correo) %>
			</asp:TableCell>--%>
		</asp:TableRow>
	</asp:Table>
	<asp:Table ID="Table1" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Imagen) %>
			</asp:TableCell>
			<asp:TableCell>
				<asp:FileUpload ID="TImagen" runat="server" />
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<hr />
	<asp:Panel ID="PanelBotones" runat="server" HorizontalAlign="Center" CssClass="PanelBotones">
		<%: Html.ActionLink("Guardar", "GuardarMesa", "Formularios") %>
		<%: Html.ActionLink("Editar", "GuardarMesa", "Formularios") %>
	</asp:Panel>

</asp:Content>
