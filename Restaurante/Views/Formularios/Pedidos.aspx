﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.DetallePedido>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <title>Pedidos</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:Menu ID="Menu" runat="server" StaticSelectedStyle-BackColor="#539DC2" StaticSelectedStyle-ForeColor="White"
		Orientation="Horizontal" CssClass="Pestañas">
		<Items>
			<asp:MenuItem Text="Registrar pedidos" Value="1"></asp:MenuItem>
		</Items>
	</asp:Menu>
	<hr />
	<asp:Table ID="TablaPedidos" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Cliente) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Cliente, new SelectList(
					new List<Object>{
						new { value = 0, text = "Anibal Molina"},
						new { value = 1, text = "Jaime Hurtatiz"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.NombrePlato) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.NombrePlato, new SelectList(
					new List<Object>{
						new { value = 0, text = "Pollo Broaster"},
						new { value = 1, text = "Pechuga gratinada"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Cantidad) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Cantidad) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.ValorUnitario) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.ValorUnitario) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.ValorTotal) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.ValorTotal) %>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<hr />
	<asp:Panel ID="PanelBotones" runat="server" HorizontalAlign="Center" CssClass="PanelBotones">
		<%: Html.ActionLink("Guardar", "Pedidos", "Formularios") %>
		<%: Html.ActionLink("Editar", "Pedidos", "Formularios") %>
		<%: Html.ActionLink("Eliminar", "Pedidos", "Formularios") %>
	</asp:Panel>
</asp:Content>
