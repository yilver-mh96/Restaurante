﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.Mesa>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Mesas</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Menu ID="Menu" runat="server" StaticSelectedStyle-BackColor="#539DC2" StaticSelectedStyle-ForeColor="White"
		Orientation="Horizontal" CssClass="Pestañas">
		<Items>
			<asp:MenuItem Text="Información general" Value="1"></asp:MenuItem>
		</Items>
	</asp:Menu>
	<hr />
	<asp:Table ID="TablaDatosPersonales" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Numero) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Numero) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Puestos) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Puestos) %>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<hr />
	<asp:Panel ID="PanelBotones" runat="server" HorizontalAlign="Center" CssClass="PanelBotones">
		<%: Html.ActionLink("Guardar", "GuardarMesa", "Formularios")%>
		<%: Html.ActionLink("Editar", "GuardarMesa", "Formularios") %>
	</asp:Panel>
	<center>
		
		<h4><%: ViewBag.Message %></h4>
		<%--<h4><%: Model.Numero%></h4>--%>
	</center>
</asp:Content>
