﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.Ventas>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Ventas</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Menu ID="Menu" runat="server" StaticSelectedStyle-BackColor="#539DC2" StaticSelectedStyle-ForeColor="White"
		Orientation="Horizontal" CssClass="Pestañas">
		<Items>
			<asp:MenuItem Text="Registrar ventas" Value="1"></asp:MenuItem>

		</Items>
	</asp:Menu>
	<hr />
	<asp:Table ID="TablaCompras" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Fecha) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Fecha) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Hora) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Hora) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Pedido) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Pedido, new SelectList(
					new List<Object>{
						new { value = 0, text = "1"},
						new { value = 1, text = "2"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Cliente) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Cliente, new SelectList(
					new List<Object>{
						new { value = 0, text = "Anibal Molina"},
						new { value = 1, text = "Jaime Hurtatiz"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Subtotal) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Subtotal) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.ValorTotal) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.ValorTotal) %>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<hr />
	<asp:Panel ID="PanelBotones" runat="server" HorizontalAlign="Center" CssClass="PanelBotones">
		<%: Html.ActionLink("Guardar", "Ventas", "Formularios") %>
		<%: Html.ActionLink("Editar", "Ventas", "Formularios") %>
		<%: Html.ActionLink("Eliminar", "Ventas", "Formularios") %>
	</asp:Panel>
</asp:Content>
