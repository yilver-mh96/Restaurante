﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.Restaurante>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	<title>Restaurante</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%--	<h2><%: ViewBag.Message %></h2>--%>
	<asp:Menu ID="Menu" runat="server" StaticSelectedStyle-BackColor="#539DC2" StaticSelectedStyle-ForeColor="White"
		Orientation="Horizontal" CssClass="Pestañas">
		<Items>
			<asp:MenuItem Text="Información general" Value="1"></asp:MenuItem>
		</Items>
	</asp:Menu>
	<hr />
	<asp:Table ID="TablaDatosPersonales" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.NombreRestaurante) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.NombreRestaurante) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Telefono) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Telefono) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Direccion) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Direccion) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Correo) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.TextBoxFor(m => m.Correo) %>
			</asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Departamento) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Departamento, new SelectList(
					new List<Object>{
						new { value = 0, text = "Caquetá"},
						new { value = 1, text = "Huila"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Ciudad) %>
			</asp:TableCell>
			<asp:TableCell>
				<%: Html.DropDownListFor(m => m.Ciudad, new SelectList(
					new List<Object>{
						new { value = 0, text = "Suaza"},
						new { value = 1, text = "Neiva"}
					},
					"value",
					"text",
					2)) %>
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<asp:Table ID="Table1" runat="server" CssClass="TablaContenidoForm">
		<asp:TableRow>
			<asp:TableCell></asp:TableCell>
		</asp:TableRow>
		<asp:TableRow>
			<asp:TableCell>
				<%: Html.LabelFor(m => m.Imagen) %>
			</asp:TableCell>
			<asp:TableCell>
				<asp:FileUpload ID="TImagen" runat="server" />
			</asp:TableCell>
		</asp:TableRow>
	</asp:Table>
	<hr />
	<asp:Panel ID="PanelBotones" runat="server" HorizontalAlign="Center" CssClass="PanelBotones">
		<%: Html.ActionLink("Guardar", "GuardarMesa", "Formularios") %>
		<%: Html.ActionLink("Editar", "GuardarMesa", "Formularios") %>
	</asp:Panel>

</asp:Content>
