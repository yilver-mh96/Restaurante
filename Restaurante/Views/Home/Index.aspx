﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Restaurante.Models.Clases.Persona>" %>

<!DOCTYPE html>

<html>
<head runat="server">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Altos de Suaza - Restaurante</title>
	<link href="~/Content/Estilos/Inicio.css" rel="stylesheet" />
	<link href="~/Content/Images/Logos/Logo.png" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
	<form id="form1" runat="server">
		<asp:Panel ID="PanelContenido" runat="server" CssClass="PanelContenido">
			<asp:Panel ID="PanelNavegacion" runat="server" CssClass="PanelNavegacion">
				<asp:Table ID="TablaNavegacion" runat="server" CssClass="TablaNavegacion" HorizontalAlign="Right">
					<asp:TableRow>
						<asp:TableCell>
							<asp:HyperLink ID="Empresa" runat="server" NavigateUrl="~/Inicio.aspx">La empresa</asp:HyperLink>
							<asp:Label ID="S2" runat="server" Text=" | "></asp:Label>
						</asp:TableCell>
						<asp:TableCell>
							<asp:HyperLink ID="Contacto" runat="server" NavigateUrl="~/Inicio.aspx">Contacto</asp:HyperLink>
							<asp:Label ID="S1" runat="server" Text=" | "></asp:Label>
						</asp:TableCell>
						<asp:TableCell>
							<asp:HyperLink ID="Menu" runat="server" NavigateUrl="~/Inicio.aspx">Menu del día</asp:HyperLink>
						</asp:TableCell>
					</asp:TableRow>
				</asp:Table>
			</asp:Panel>
			<asp:Panel ID="PanelImagen" runat="server" CssClass="PanelImagen">
				<asp:Image ID="Imagen" runat="server" CssClass="Imagen" ImageUrl="~/Content/Images/img4.jpg" />
			</asp:Panel>
			<asp:Panel ID="PanelLogo" runat="server" CssClass="PanelLogo">
				<asp:Panel ID="Panel1" runat="server">
					<asp:Table ID="TablaLogo" runat="server">
						<asp:TableRow>
							<asp:TableCell>
								<asp:Image ID="Logo" runat="server" ImageUrl="~/Content/Images/Logos/Logo.png" CssClass="Logo" />
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell>
								<hr />
								<asp:Label ID="Titulo" runat="server" Text="Altos de Suaza"></asp:Label>
								<br />
								<asp:Label ID="Subtitulo" runat="server" Text="Restaurante"></asp:Label>
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell>
								<asp:ImageButton ID="Login" runat="server" ImageUrl="~/Content/Images/Iconos/down.png"></asp:ImageButton>
							</asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:Panel>
				<asp:Panel ID="PanelSesion" runat="server" CssClass="PanelSesion" Visible="true">
					<asp:Table ID="TablaContenido" runat="server" CssClass="TablaSesion">
						<asp:TableRow>
							<asp:TableCell>
								<br />
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell HorizontalAlign="Right">
								<%: Html.LabelFor(m => m.NUsuario) %>
							</asp:TableCell>
							<asp:TableCell>
								<%: Html.TextBoxFor(m => m.NUsuario) %>
								<%: Html.ValidationMessageFor(m => m.NUsuario) %>
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell>
								<%: Html.LabelFor(m => m.Contrasenia) %>
							</asp:TableCell>
							<asp:TableCell>
								<%: Html.PasswordFor(m => m.Contrasenia) %>
								<%: Html.ValidationMessageFor(m => m.Contrasenia) %>
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell Visible="false">
								<%: Html.CheckBox("Recordar")%>Recordar
							</asp:TableCell>
						</asp:TableRow>
						<asp:TableRow>
							<asp:TableCell ColumnSpan="2">
								<br />
								<%: Html.ActionLink("Iniciar sesión", "IniciarSesion", "Home") %>
							</asp:TableCell>
						</asp:TableRow>
					</asp:Table>
				</asp:Panel>
			</asp:Panel>
		</asp:Panel>
	</form>
</body>
</html>
