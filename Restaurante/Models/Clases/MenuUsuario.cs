﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Threading;
using Restaurante.Models.Datos;

namespace Restaurante.Models.Clases {
	public class MenuUsuario {

		public MenuUsuario() {
		}

		[Required]
		[Display(Name = "idMenu")]
		public string idMenu { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Rol")]
		public string Rol { get; set; }


		//public void CargarMenuUsuario(Menu MyMenu) {
		//	try {
		//		NombreUsuario.Text = Session["NombreUsuario"].ToString();
		//		DataTable datos = LN_Persona.MostrarTabla("organizacion");
		//		DataRow f = datos.Rows[0];
		//		Nombre.Text = f["Nombre"].ToString();

		//			if (Session["Inicio"].ToString().Equals("Activo")) {
		//				DataTable dt = LN_Persona.CargarMenu(Session["TipoPersona"].ToString());
		//				//DataTable dt = LN_Persona.CargarMenu("1");
		//				if (dt.Rows.Count > 0) {
		//					DataRow fila;
		//					for (int i = 0; i < dt.Rows.Count; i++) {
		//						fila = dt.Rows[i];
		//						MenuItem mt = new MenuItem();
		//						mt.Text = fila["NombrePermiso"].ToString();
		//						mt.NavigateUrl = fila["Pagina"].ToString() + ".aspx";
		//						mt.ImageUrl = "Images/" + fila["Imagen"].ToString();

		//						MyMenu.Items.Add(mt);
		//					}
		//				}
		//			}
		//	} catch (Exception ex) {

		//		//this.MostrarMensaje("Error al cargar la páginca. \nEs posible que la sesión haya caducado. Ingresa nuevamente.", this.Page);
		//	}
		//}


		public DataTable MostrarCiudad(string dpto) {
			List<MySqlParameter> lista = new List<MySqlParameter>();
			string sql = "SELECT m.NombreMpio FROM municipio m INNER JOIN departamento d ON d.NombreDpto='" + dpto + "' AND m.FK_idDpto=d.idDpto;";
			return Conexion.EjecutarConsulta(sql, lista, System.Data.CommandType.Text);
		}
	}
}