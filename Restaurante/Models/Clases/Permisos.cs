﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Restaurante.Models.Clases {
	public class Permisos {

		public Permisos() {
		}

		[Required]
		[Display(Name = "Id permiso")]
		public long idPermiso { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Permiso")]
		public string Permiso { get; set; }
	}
}