﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Threading;
using Restaurante.Models.Datos;


namespace Restaurante.Models.Clases {
	public class Proveedor {

		public Proveedor() {
		}

		[Required]
		[Display(Name = "Identificación")]
		public long idProveedor { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Proveedor")]
		public string NombreProveedor { get; set; }
	}
}