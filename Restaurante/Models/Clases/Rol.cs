﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Restaurante.Models.Clases {
	public class Rol {

		public Rol() {
		}

		[Required]
		[Display(Name = "Id rol")]
		public long idRol { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Role")]
		public string NombreRol { get; set; }
	}
}