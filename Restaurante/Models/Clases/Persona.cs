﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Threading;
using Restaurante.Models.Datos;

namespace Restaurante.Models.Clases {
	public class Persona : IPersona {

		public Persona() {
		}

		[Required]
		[Display(Name = "Identificación")]
		public long Cedula { get; set; }

		[Required]
		[Display(Name = "Rol")]
		public long Rol { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Primer nombre")]
		public string Nombre1 { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Segundo nombre")]
		public string Nombre2 { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Primer apellido")]
		public string Apellido1 { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Segundo apellido")]
		public string Apellido2 { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Género")]
		public string Genero { get; set; }

		[Required]
		[DataType(DataType.Date)]
		[Display(Name = "Fecha nacimiento")]
		public string FechaNac { get; set; }

		[Required]
		[Display(Name = "Direccion")]
		public string Direccion { get; set; }

		[Required]
		[DataType(DataType.PhoneNumber)]
		[Display(Name = "Teléfono")]
		public long Telefono { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Profesión")]
		public string Profesion { get; set; }

		[Required]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Correo")]
		public string Correo { get; set; }

		[Required]
		[Display(Name = "Usuario")]
		public string NUsuario { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Contraseña")]
		public string Contrasenia { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Confirmar")]
		[Compare("Contrasenia", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
		public string ConfirmContrasenia { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Ciudad")]
		public string Ciudad { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Departamento")]
		public string Departamento { get; set; }

		public string MostrarPersona(Persona obj) {
			return obj.Cedula + " " + obj.Nombre1 + " " + obj.Apellido1;
		}

		public bool ValidarUsuario(Persona obj) {
			List<MySqlParameter> lista = new List<MySqlParameter>();
			string sql = "SELECT * FROM cuenta c INNER JOIN persona p ON c.NombreUsuario='" + obj.NUsuario + "' AND c.Contrasenia='" + obj.Contrasenia + "' AND c.FK_idPersona=p.idPersona;";
			DataTable dt = Conexion.EjecutarConsulta(sql, lista, System.Data.CommandType.Text);
			return (dt.Rows.Count > 0) ? true : false;
		}

		public DataTable ConsultarMunicipios (string dpto) {
			List<MySqlParameter> lista = new List<MySqlParameter>();
			string sql = "SELECT * FROM municipio m INNER JOIN departamento d ON m.FK_idDpto='" + dpto + "' AND m.FK_idDpto=d.idDpto";
			return Conexion.EjecutarConsulta(sql, lista, System.Data.CommandType.Text);
		}
	}
}
