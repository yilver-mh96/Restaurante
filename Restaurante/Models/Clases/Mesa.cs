﻿using Restaurante.Models.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;


namespace Restaurante.Models.Clases {
	public class Mesa {

		public Mesa() {
		}

		[Required]
		[Display(Name = "N° mesa")]
		public int Numero { get; set; }

		[Required]
		[Display(Name = "Capacidad")]
		public int Puestos { get; set; }

		[Required]
		[Display(Name = "Estado")]
		public string Estado { get; set; }

		public bool RegistrarMesa(Mesa obj) {
			string sql = "INSERT INTO mesa(Numero, Puestos, Estado) VALUES('{0}','{1}','{2}')";
			sql = string.Format(sql, obj.Numero, obj.Puestos, obj.Estado);
			string[] ar = new string[1];
			ar[0] = sql;
			return Conexion.RealizarTransaccion(ar);
		}
	}
}