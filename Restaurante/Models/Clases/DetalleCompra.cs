﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Threading;
using Restaurante.Models.Datos;

namespace Restaurante.Models.Clases {
	public class DetalleCompra : Compras {

		public DetalleCompra() {
		}

		[Required]
		[Display(Name = "idDetalleCompra")]
		public long idDCompra { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Cantidad")]
		public int Cantidad { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Valor unitario")]
		public long ValorUnitario { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "TotalDetalle")]
		public long TotalDetalle { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Producto")]
		public string NombreProducto { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Unidad presentación")]
		public string Presentacion { get; set; }
	}
}