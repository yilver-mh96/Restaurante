﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Restaurante.Models.Clases {

	public class Plato {

		public Plato() {
		}

		[Required]
		[Display(Name = "idPlato")]
		public long idPlato { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Nombre del plato")]
		public string NombrePlato { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Tipo de plato")]
		public string TipoPlato { get; set; }

		[Required]
		[Display(Name = "Imagen del plato")]
		public string Imagen { get; set; }

		[Required]
		[Display(Name = "Precio")]
		public long Precio { get; set; }
	}
}