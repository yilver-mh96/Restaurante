﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Restaurante.Models.Clases {
	public class Subpermisos {

		public Subpermisos() {
		}

		[Required]
		[Display(Name = "Id subpermiso")]
		public long idSubpermiso { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Subpermiso")]
		public string Subpermiso { get; set; }
	}
}