﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Threading;
using Restaurante.Models.Datos;
namespace Restaurante.Models.Clases {
	public class Ventas {

		public Ventas() {
		}

		[Required]
		[Display(Name = "idVenta")]
		public long idVenta { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Fecha")]
		public string Fecha { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Hora")]
		public string Hora { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Subtotal")]
		public string Subtotal { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Valor total")]
		public string ValorTotal { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Estado venta")]
		public string Estado { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Pedido")]
		public string Pedido { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Cliente")]
		public string Cliente { get; set; }
	}
}