﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Restaurante.Models.Clases {
	public class Restaurante {

		public Restaurante() {
		}

		[Required]
		[Display(Name = "idRestaurante")]
		public long idRestaurante { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Nombre")]
		public string NombreRestaurante { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Dirección")]
		public string Direccion { get; set; }

		[Required]
		[Display(Name = "Telefono")]
		public string Telefono { get; set; }

		[Required]
		[DataType(DataType.EmailAddress)]
		[Display(Name = "Correo")]
		public long Correo { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Ciudad")]
		public long Ciudad { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Departamento")]
		public long Departamento { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Imagen")]
		public long Imagen { get; set; }
	}
}