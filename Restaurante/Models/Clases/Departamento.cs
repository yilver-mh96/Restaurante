﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Threading;
using Restaurante.Models.Datos;

namespace Restaurante.Models.Clases {
	public class Departamento {

		public Departamento() {
		}

		[Required]
		[Display(Name = "Id Dpto")]
		public int idDpto { get; set; }

		[Required]
		[DataType(DataType.Text)]
		[Display(Name = "Departamento")]
		public string NombreDpto { get; set; }

		public DataTable ConsultarDepartamentos() {
			List<MySqlParameter> lista = new List<MySqlParameter>();
			string sql = "SELECT * FROM departamento";
			return Conexion.EjecutarConsulta(sql, lista, System.Data.CommandType.Text);
		}
	}
}