﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Restaurante.Models.Datos
{
    public class Conexion
    {
        private static MySqlConnection conexion = new MySqlConnection(ConfigurationManager.ConnectionStrings["conexion_mysql"].ConnectionString);
        private static bool Conectar()
        {
            try
            {
                conexion.Open();
                return true;
            }
            catch (Exception)
            {

                return false;
            }

        }
        private static void Desconectar()
        {
            conexion.Close();
        }

        public static void EjecutarOperacion(string sentencia, List<MySqlParameter> ListaParametros, CommandType TipoComando)
        {
            if (Conectar())
            {
                MySqlCommand comando = new MySqlCommand();
                comando.CommandText = sentencia;
                comando.CommandType = TipoComando;
                comando.Connection = conexion;
                foreach (MySqlParameter parametro in ListaParametros)
                {
                    comando.Parameters.Add(parametro);
                }
                comando.ExecuteNonQuery();
                Desconectar();
                throw new Exception("¡Operación exitosa!");
            }
            else
            {
                throw new Exception("No ha sido posible conectarse a la Base de Datos");
            }
        }
        public static void EjecutarOper(string sentencia, List<MySqlParameter> ListaParametros, CommandType TipoComando, MySqlDbType df)
        {
            if (Conectar())
            {
                MySqlCommand comando = new MySqlCommand();
                comando.CommandText = sentencia;
                comando.CommandType = TipoComando;
                comando.Connection = conexion;

                foreach (MySqlParameter parametro in ListaParametros)
                {
                    comando.Parameters.Add(parametro);
                }
                comando.ExecuteNonQuery();
                Desconectar();
                throw new Exception("¡Operación exitosa!");
            }
            else
            {
                throw new Exception("No ha sido posible conectarse a la Base de Datos");
            }
        }

        public static DataTable EjecutarConsulta(string sentencia, List<MySqlParameter> ListaParametros, CommandType TipoComando)
        {
            MySqlDataAdapter adaptador = new MySqlDataAdapter();
            adaptador.SelectCommand = new MySqlCommand(sentencia, conexion);
            adaptador.SelectCommand.CommandType = TipoComando;

            foreach (MySqlParameter parametro in ListaParametros)
            {
                adaptador.SelectCommand.Parameters.Add(parametro);
            }
            DataSet resultado = new DataSet();
            adaptador.Fill(resultado);

            return resultado.Tables[0];
        }

        public static DataSet ConsultarGraficos(string sentencia, List<MySqlParameter> ListaParametros, CommandType TipoComando)
        {
            MySqlDataAdapter adaptador = new MySqlDataAdapter();
            adaptador.SelectCommand = new MySqlCommand(sentencia, conexion);
            adaptador.SelectCommand.CommandType = TipoComando;

            foreach (MySqlParameter parametro in ListaParametros)
            {
                adaptador.SelectCommand.Parameters.Add(parametro);
            }
            DataSet resultado = new DataSet();
            adaptador.Fill(resultado);

            return resultado;
        }

        public string cad_Conexion()
        {
            return ConfigurationManager.ConnectionStrings["conexion_mysql"].ConnectionString;

        }

        public MySqlConnection ConectarMysql() //Metoto para conectar a C# a MySQL
        {
            string CadenaConexion;
            CadenaConexion = cad_Conexion();
            MySqlConnection Conexion = new MySqlConnection(CadenaConexion);
            try
            {
                Conexion.Open();
            }
            catch (Exception error)
            {
                //  MessageBox.Show("Error de configuración del sistema " + error.Message, "Aplicación : ", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //   Application.Exit();
            }
            return Conexion;
        }


        public DataSet EjecutarSelectMysql(string p)
        {
            System.Data.DataSet dt = new System.Data.DataSet();

            MySqlConnection Conn = ConectarMysql();
            MySqlCommand ComandoSelect = new MySqlCommand(p);
            ComandoSelect.Connection = Conn;

            // MySqlDataReader Resultado;
            MySqlDataAdapter da = new MySqlDataAdapter(p, Conn);
            //da = ComandoSelect.ExecuteReader();
            da.Fill(dt);
            Conn.Close();
            return dt;
        }
        
        public static bool RealizarTransaccion(string[] cadena)
        {
            MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["conexion_mysql"].ConnectionString);
            con.Open();
            MySqlTransaction Transa = con.BeginTransaction();
            MySqlCommand cmd;
            try
            {
                for (int i = 0; i < cadena.Length; i++)
                {
                    if (cadena[i].Length > 0)
                    {
                        cmd = new MySqlCommand(cadena[i], con);
                        cmd.Transaction = Transa;
                        cmd.ExecuteNonQuery();
                    }
                }
                Transa.Commit();
                con.Close();
                return true;
            }
            catch
            {
                Transa.Rollback();
                con.Close();
                return false;
            }
        }
    }
}
