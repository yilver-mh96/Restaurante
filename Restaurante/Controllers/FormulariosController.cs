﻿using Restaurante;
using Restaurante.Models.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Web.UI.WebControls;

namespace Restaurante.Controllers {
	public class FormulariosController : Controller {
		static int cont = 0;

		Persona usuario = new Persona();
		Departamento depto = new Departamento();

		public SelectList ConsultarDepartamentos() {
			DataTable dt = depto.ConsultarDepartamentos();

			var dpto = new List<Departamento>();
			DataRow fila;
			for (int i = 0; i < dt.Rows.Count; i++) {
				fila = dt.Rows[i];
				dpto.Add(new Departamento() {
					idDpto = Convert.ToInt32(fila["idDpto"].ToString()),
					NombreDpto = fila["NombreDpto"].ToString()
				});
			}
			return new SelectList(dpto, "idDpto", "NombreDpto");
		}

		//Persona-----------------------------------------------------------------------
		public ActionResult Usuario() {
			usuario.Cedula = 1080364988;
			usuario.Nombre1 = "Yilver";
			usuario.Apellido1 = "Molina";
			ViewBag.Message = usuario.MostrarPersona(usuario);

			ViewData["dpto"] = ConsultarDepartamentos();

			return View();
		}

		public ActionResult Index() {
			ViewBag.Message = "Hola buenos días";
			return View();
		}

		public ActionResult Hola() {
			ViewBag.Message = "Hola buenos días";
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult Usuario(Persona model) {
			string user;
			user = model.Nombre1;
			ViewData["dpto"] = ConsultarDepartamentos();
			ViewBag.Message = "Operación realizada";
			return View(model);
		}

		public ActionResult MostrarCiudades() {
			MenuUsuario obj = new MenuUsuario();
			string res = "", dpto;
			dpto = "Huila";
			DataTable dt = obj.MostrarCiudad(dpto);
			if (dt.Rows.Count > 0) {
				DataRow fila;
				for (int i = 0; i < dt.Rows.Count; i++) {
					fila = dt.Rows[i];
					res += fila["NombreMpio"].ToString();
				}
			}
			ViewBag.Message = res;
			return View("Usuario");
		}

		//Restaurante----------------------------------------------------------------------
		public ActionResult Restaurante() {
			ViewBag.Message = "Hola buenos días";
			return View();
		}

		//Mesas----------------------------------------------------------------------
		public ActionResult Mesas() {
			ViewBag.Message = "Registro de mesas";
			return View();
		}

		public ActionResult GuardarMesa() {
			Mesa obj = new Mesa();
			obj.Numero = 1;
			obj.Puestos = 8;
			obj.Estado = "1";
			ViewBag.Message = (obj.RegistrarMesa(obj)) ? "Mesa registrada correctamente" : "Fallo en el registro";
			return View("Mesas", obj);
		}

		//Platos----------------------------------------------------------------------
		public ActionResult Platos() {
			return View();
		}

		//Pedidos----------------------------------------------------------------------
		public ActionResult Pedidos() {
			return View();
		}

		//Compras----------------------------------------------------------------------
		public ActionResult Compras() {
			return View();
		}

		//Ventas----------------------------------------------------------------------
		public ActionResult Ventas() {
			return View();
		}

		//Clientes----------------------------------------------------------------------
		public ActionResult Clientes() {
			return View();
		}

		//Productos----------------------------------------------------------------------
		public ActionResult Productos() {
			return View();
		}

		//MenuUsuario----------------------------------------------------------------------
		public ActionResult Menu() {
			return View();
		}

	}
}
